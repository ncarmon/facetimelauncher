//
//  AppDelegate.swift
//  FaceTimeLauncher
//
//  Created by Nava Carmon on 12/2/18.
//  Copyright © 2018 Memomi. All rights reserved.
//

import Cocoa
import CocoaAsyncSocket

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var textView: NSTextView!
    
    var socketQueue: DispatchQueue = DispatchQueue(label: "socketQueue")
    var connectedSockets: Array<GCDAsyncSocket> = Array<GCDAsyncSocket>()
    var listenSocket: GCDAsyncSocket?
    
    var isRunning: Bool = false

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
//        runFaceTimeScript("+972528157770")
//        sleep(10)
//        endFaceTimeScript()
//        sleep(10)
//        runFaceTimeScript("+972528157770")
//        sleep(10)
//        endFaceTimeScript()
        listenSocket = GCDAsyncSocket(delegate: self, delegateQueue: socketQueue)
        if listenSocket != nil {
            guard (try? listenSocket?.accept(onPort: 8888)) != nil else {
                print("Cannot open socket on port 8888")
                return
            }
            isRunning = true
            textView.append(string:"Server is up and running...\nListening on port 8888\n")
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
        if isRunning {
            listenSocket?.disconnect()
        }
    }
    
    func runFaceTimeScript(_ phoneNumber: String) {
        guard let url = URL(string:"https://s3-us-west-2.amazonaws.com/memomi-data/FaceTimeScript.txt") else {return}
        
        var myAppleScript: String?
        do {
            let text2 = try String(contentsOf: url, encoding: .utf8)
            myAppleScript = String(format:text2, phoneNumber)
        }
        catch {}
        
        guard let appleScript = myAppleScript else {return}
        var error: NSDictionary?
        if let scriptObject = NSAppleScript(source: appleScript) {
            if let outputString = scriptObject.executeAndReturnError(&error).stringValue {
                print(outputString)
            } else if (error != nil) {
                print("error: ", error!)
            }
        }
    }
    
    func endFaceTimeScript() {
        let myAppleScript =
//        "tell the application \"FaceTime\"\n" +
//            "activate\n" +
//        "end tell\n" +
//        "tell application \"System Events\"\n" +
//        "repeat while not (button \"End\" of window 1 of application process \"FaceTime\" exists)\n" +
//        "delay 1\n" +
//        "end repeat\n" +
//        "click button \"End\" of window 1 of application process \"FaceTime\"\n" +
//        "end tell" +
        "tell the application \"FaceTime\"\n" +
        "quit\n" +
        "end tell"

        var error: NSDictionary?
        if let scriptObject = NSAppleScript(source: myAppleScript) {
            if let outputString = scriptObject.executeAndReturnError(&error).stringValue {
                print(outputString)
            } else if (error != nil) {
                print("error: ", error!)
            }
        }

    }

    func synced(_ lock: Any, closure: () -> ()) {
        objc_sync_enter(lock)
        closure()
        objc_sync_exit(lock)
    }

}

let readTimeout = 15.0
let readTimeoutExtension = 10.0
let welcomeMsgTag = 0
let echoMsgTag = 1
let warningMsgTag = 2
let startCallTag = 3
let endCallTag = 4

extension AppDelegate: GCDAsyncSocketDelegate {
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        sock.readData(withTimeout: -1, tag: echoMsgTag)
        guard let string = String(data: data, encoding: .utf8) else {
            print("Couldn't read data")
            return
        }
        print("Got the string: \(string)")
        var code: Int = 0
        var phone: String = ""
        do {
            if let jsonObject = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any> {
                code = jsonObject["code"] as! Int
                phone = jsonObject["phone"] as! String
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(error)
        }

        
        DispatchQueue.main.async {
            switch code {
            case startCallTag:
                self.runFaceTimeScript(phone)
                self.textView.append(string:"Got from server startCallTag with data \(phone)\n")
                break
            case endCallTag:
                self.endFaceTimeScript()
                self.textView.append(string:"Got from server endCallTag with data \(phone)\n")
                self.connectedSockets.forEach({ $0.disconnect()})
                self.textView.append(string:"All sockets were disconnected\n")
                break
            default:
                break
            }
            sock.write(data, withTimeout: -1, tag: echoMsgTag)
        }
        
    }
    
    func socket(_ sock: GCDAsyncSocket, didAcceptNewSocket newSocket: GCDAsyncSocket) {
        synced(connectedSockets) {
            connectedSockets.append(newSocket)
        }
        
        let message = "did accept new socket on host:\(newSocket.connectedHost ?? "No host"), and port:\(newSocket.connectedPort)"
        print(message)
        textView.append(string: message)
        let welcomeMessage = "Welcome to the AsyncSocket server"
        if let data = welcomeMessage.data(using: .utf8) {
            newSocket.write(data, withTimeout: -1, tag: 0)
            newSocket.readData(to: GCDAsyncSocket.crlfData(), withTimeout: readTimeout, tag: echoMsgTag)
            textView.append(string:"\(welcomeMessage)\n")
       }
    }
    
    func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        if tag == echoMsgTag {
            sock.readData(to: GCDAsyncSocket.crlfData(), withTimeout: readTimeout, tag: echoMsgTag)
        }
    }
    
    func socket(_ sock: GCDAsyncSocket, shouldTimeoutReadWithTag tag: Int, elapsed: TimeInterval, bytesDone length: UInt) -> TimeInterval {
        if elapsed <= readTimeout {
            let warningMsg = "Are you still there?\r\n"
            if let warningData = warningMsg.data(using: .utf8) {
                sock.write(warningData, withTimeout: -1, tag: warningMsgTag)
            }
        }
        return readTimeoutExtension
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        if sock != listenSocket {
            synced(connectedSockets) {
                if let index = connectedSockets.index(of:sock) {
                    connectedSockets.remove(at: index)
                }
            }
        }
    }
}

extension NSTextView {
    func append(string: String) {
        if Thread.current == Thread.main {
            self.textStorage?.append(NSAttributedString(string: string))
            self.scrollToEndOfDocument(nil)
        } else {
            DispatchQueue.main.async {
                self.textStorage?.append(NSAttributedString(string: string))
                self.scrollToEndOfDocument(nil)
            }
        }
    }
}


